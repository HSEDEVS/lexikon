package Observer_Joke;

import java.util.Observable;
import java.util.Observer;

class JokeListener implements Observer {
  final private String name;

  JokeListener( String name )
  {
    this.name = name;
  }

  @Override public void update( Observable o, Object arg )
  {
    System.out.println( name + " lacht �ber: \"" + arg + "\"" );
  }
}