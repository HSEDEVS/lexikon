package Observer_Joke;

import java.util.*;

class JokeTeller extends Observable
{
  private static final List<String> jokes = Arrays.asList(
    "Sorry, aber du siehst so aus, wie ich mich f�hle.",
    "Eine Null kann ein bestehendes Problem verzehnfachen.",
    "Wer zuletzt lacht, hat es nicht eher begriffen.",
    "Wer zuletzt lacht, stirbt wenigstens fr�hlich.",
    "Unsere Luft hat einen Vorteil: Man sieht, was man einatmet."
  );

  public void tellJoke()
  {
    setChanged();
    Collections.shuffle( jokes );
    notifyObservers( jokes.get(0) );
  }
}