package Observer_Chat;

import java.util.Observable;
import java.util.Observer;

public class User implements Observer{
	private String username;
	private String password;
	
	User(String u, String p){
		this.username = u;
		this.password = p;
	}
	
	public String getUser() {
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		System.out.println(((User)arg).getUser() + "wurde hinzugefügt!");
		
	}
	
	
	
}
