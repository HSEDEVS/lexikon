package Observer_Chat;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

public class Main {
		
	public static void main(String[] args) {
		String benutzername;
		String password;
		Userlist userlist = new Userlist();
		
		System.out.println("Bitte logen Sie sich ein.");
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Benutzername: ");
		benutzername = sc.nextLine();
		System.out.print("Passwort :");
		password = sc.nextLine();
		
		Observer user = new User(benutzername,password);
		userlist.addObserver(user);
		
		if(userlist.userInList(user)) {
			System.out.println("Sie werden eingeloggt.");
		} else {
			System.out.println("User nicht vorhanden. User wird registriert.");
			userlist.addUser(user);
		}	
	

	}

	
	
	

}
