package Observer_Chat;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Userlist extends Observable {
	private ArrayList<User> userlist = new ArrayList<User>();
	
	Userlist(){
		//addObserver(o);
	}
	
	public ArrayList<User> getUserlist() {
		return this.userlist;
	}
	
	public boolean userInList(Observer o) {
		return userlist.contains((User)o);
	}
	
	public void addUser(Observer o) {
		System.out.println();
		userlist.add(((User)o));
		
		setChanged();
		notifyObservers(o);
		
	}
	
	public void removeUser(User u) {
		userlist.remove(userlist.indexOf(u));
	}
	
	public void clearList() {
		userlist.clear();
	}
	
}
